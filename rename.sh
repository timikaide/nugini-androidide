#!/bin/sh
set -e

PROGNAME=$(basename $0)
WORKING_DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
OLD_TITLE="DTStart"
OLD_PACKAGE="old.package.name"

die() {
    echo "$PROGNAME: $*" >&2
    exit 1
}

usage() {
    if [ "$*" != "" ] ; then
        echo "Error: $*"
    fi

    cat << EOF
Usage: $PROGNAME --package-name [PACKAGE_NAME] --title [TITLE]
Rename an Android app and package.

Options:
-h, --help                         display this usage message and exit
-p, --package-name [PACKAGE_NAME]  new package name (i.e. com.example.package)
-t, --title [TITLE]                new app title (i.e. MyApp)
EOF

    exit 1
}

packagename=""
title=""
while [ $# -gt 0 ] ; do
    case "$1" in
    -h|--help)
        usage
        ;;
    -p|--package-name)
        packagename="$2"
        shift
        ;;
    -t|--title)
        title="$2"
        shift
        ;;
    -*)
        usage "Unknown option '$1'"
        ;;
    *)
        usage "Too many arguments"
      ;;
    esac
    shift
done

if [ -z "$packagename" ] ; then
    usage "Not enough arguments"
fi

if [ -z "$title" ] ; then
    usage "Not enough arguments"
fi

TITLE_NO_SPACES="${title// /}"

# get rid of the git history
rm -rf ./.git

# Rename main folder
mv $OLD_TITLE $TITLE_NO_SPACES

# Rename folder structure
renamefolderstructure() {
  DIR=""
  if [ "$*" != "" ] ; then
      DIR="$*"
  fi
  ORIG_DIR=$DIR

  mv $TITLE_NO_SPACES/$DIR/old/package/name $TITLE_NO_SPACES/$DIR/
  rmdir $TITLE_NO_SPACES/$DIR/old/package
  rmdir $TITLE_NO_SPACES/$DIR/old
  cd $TITLE_NO_SPACES/$DIR
  IFS='.' read -ra packages <<< "$packagename"
  for i in "${packages[@]}"; do
      DIR="$DIR/$i"
      mkdir $i
      cd $i
  done
  mv $WORKING_DIR/$TITLE_NO_SPACES/$ORIG_DIR/name/* ./
  rmdir $WORKING_DIR/$TITLE_NO_SPACES/$ORIG_DIR/name
  cd $WORKING_DIR
  echo $DIR
}

# Rename project folder structure
PACKAGE_DIR="app/src/main/java"
PACKAGE_DIR=$( renamefolderstructure $PACKAGE_DIR )

# Rename android test folder structure
ANDROIDTEST_DIR="app/src/androidTest/java"
ANDROIDTEST_DIR=$( renamefolderstructure $ANDROIDTEST_DIR )

# Rename test folder structure
TEST_DIR="app/src/test/java"
TEST_DIR=$( renamefolderstructure $TEST_DIR )

# rename application file
mv $TITLE_NO_SPACES/$PACKAGE_DIR/${OLD_TITLE}Application.java $TITLE_NO_SPACES/$PACKAGE_DIR/${TITLE_NO_SPACES}Application.java

echo "Files structure has been renamed. Replacing package and name within files..."

# search and replace in files
PACKAGE_NAME_ESCAPED="${packagename//./\.}"
OLD_PACKAGE_NAME_ESCAPED="${OLD_PACKAGE//./\.}"
LC_ALL=C find $WORKING_DIR/$TITLE_NO_SPACES -type f -exec sed -i "" "s/$OLD_PACKAGE_NAME_ESCAPED/$PACKAGE_NAME_ESCAPED/g" {} +
LC_ALL=C find $WORKING_DIR/$TITLE_NO_SPACES -type f -exec sed -i "" "s/$OLD_TITLE/$TITLE_NO_SPACES/g" {} +
